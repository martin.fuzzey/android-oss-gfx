# What is this?

It can be complicated to get an open source graphics stack running under Android.

Most of the pieces already exist in upstream projects like mesa and drm_hwcomposer but everyone does their own integration, often on a per device basis.
This is unlike the classical Linux world where distributions build a coherent set of packages.

Google's focus still seems to be on proprietary blob solutions.

This repository is an attempt to address this by pulling together the repostiories and patches needed.
Currently I am working on i.MX6 hardware using the etnaviv driver but will probably also do some i.MX53 (freedreno) and maybe i.MX8 too.
MRs welcome for other targets!

This is not a tutorial on how to build Android, see the AOSP docs for that, rather just on how to integrate the graphics stack.


# Repository structure

The first two directory levels are "target" and "android version".
Currently these are "imx6" and "android-8.1" only.

Below the directories are:

## device

Contains files or files snippets that should be included in the subdirectory of
device dedicated to your board in the android source tree.

Eg: BoardConfig.mk_ossgfx should be added to or incldued from BoardConfig.mk

## local_manifests
repo manifest files used to obtain the (unpatched) sources.

These should be added to the .repo/local_manifests directory of an Android build

## patches

patches to apply to the upstream source.

The directory names replace "/" by "_". So external_mesa3d should be applied to the external/mesa3d directory

Each directory contains a series file and a set of patch files.

One way to apply them is to use "stgit"

Eg
~~~
repo start external/mesa3d my_work_branch
cd external/mesa3d
stg init
stg import -s PATH_TO_CLONE_OF_THIS_REPO/imx6/android-8.1/patches/external_mesa3d/series
~~~
