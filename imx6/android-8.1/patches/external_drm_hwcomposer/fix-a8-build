Fix build on Android 8.

From: Martin Fuzzey <martin.fuzzey@flowbird.group>
Date: 2020-01-15 16:14:08 +0100 

A mixed bag of fairly minro fixes.

define DHWC2_USE_OLD_GB_IMPORT to support android 8.
This used to done automatically depending on the Android version but it was
lost in the Android.mk => Android.bp transition

Disable hwcomposer.drm_minigbm as we don't have its dependencies

Fix a couple of instances of "suggest braces around initialization of subobject"
warnings that lead to errors due to -Wall.

Use conditional compilation to disable features requiring hw_composer 2.3 headers

Fix a unsigned / signed comparison leading to error due to -Wall
---
 Android.bp                      |    2 ++
 drm/drmconnector.cpp            |    4 ++--
 drmhwctwo.cpp                   |    6 ++++--
 platform/platformdrmgeneric.cpp |    2 +-
 4 files changed, 9 insertions(+), 5 deletions(-)

diff --git a/Android.bp b/Android.bp
index 85be0b5..3925d1c 100644
--- a/Android.bp
+++ b/Android.bp
@@ -59,6 +59,7 @@ cc_defaults {
     cppflags: [
         "-DHWC2_USE_CPP11",
         "-DHWC2_INCLUDE_STRINGIFICATION",
+        "-DHWC2_USE_OLD_GB_IMPORT", // Needed for Android < 9
     ],
 
     relative_install_path: "hw",
@@ -101,6 +102,7 @@ cc_library_shared {
 
 cc_library_shared {
     name: "hwcomposer.drm_minigbm",
+    enabled: false,
     defaults: ["hwcomposer.drm_defaults"],
     whole_static_libs: ["drm_hwcomposer"],
     srcs: [
diff --git a/drm/drmconnector.cpp b/drm/drmconnector.cpp
index db3f9b6..d4b766f 100644
--- a/drm/drmconnector.cpp
+++ b/drm/drmconnector.cpp
@@ -125,9 +125,9 @@ bool DrmConnector::valid_type() const {
 
 std::string DrmConnector::name() const {
   constexpr std::array<const char *, TYPES_COUNT> names =
-      {"None",   "VGA",  "DVI-I",     "DVI-D",   "DVI-A", "Composite",
+      {{"None",   "VGA",  "DVI-I",     "DVI-D",   "DVI-A", "Composite",
        "SVIDEO", "LVDS", "Component", "DIN",     "DP",    "HDMI-A",
-       "HDMI-B", "TV",   "eDP",       "Virtual", "DSI"};
+       "HDMI-B", "TV",   "eDP",       "Virtual", "DSI"}};
 
   if (type_ < TYPES_COUNT) {
     std::ostringstream name_buf;
diff --git a/drmhwctwo.cpp b/drmhwctwo.cpp
index 33ad0fb..e0097be 100644
--- a/drmhwctwo.cpp
+++ b/drmhwctwo.cpp
@@ -236,10 +236,10 @@ DrmHwcTwo::HwcDisplay::HwcDisplay(ResourceManager *resource_manager,
   supported(__func__);
 
   // clang-format off
-  color_transform_matrix_ = {1.0, 0.0, 0.0, 0.0,
+  color_transform_matrix_ = {{1.0, 0.0, 0.0, 0.0,
                              0.0, 1.0, 0.0, 0.0,
                              0.0, 0.0, 1.0, 0.0,
-                             0.0, 0.0, 0.0, 1.0};
+                             0.0, 0.0, 0.0, 1.0}};
   // clang-format on
 }
 
@@ -1315,6 +1315,7 @@ hwc2_function_pointer_t DrmHwcTwo::HookDevGetFunction(
       return ToHook<HWC2_PFN_VALIDATE_DISPLAY>(
           DisplayHook<decltype(&HwcDisplay::ValidateDisplay),
                       &HwcDisplay::ValidateDisplay, uint32_t *, uint32_t *>);
+#ifdef HWC2_API_2_3
     case HWC2::FunctionDescriptor::GetDisplayIdentificationData:
       return ToHook<HWC2_PFN_GET_DISPLAY_IDENTIFICATION_DATA>(
           DisplayHook<decltype(&HwcDisplay::GetDisplayIdentificationData),
@@ -1325,6 +1326,7 @@ hwc2_function_pointer_t DrmHwcTwo::HookDevGetFunction(
           DisplayHook<decltype(&HwcDisplay::GetDisplayCapabilities),
                       &HwcDisplay::GetDisplayCapabilities, uint32_t *,
                       uint32_t *>);
+#endif
 
     // Layer functions
     case HWC2::FunctionDescriptor::SetCursorPosition:
diff --git a/platform/platformdrmgeneric.cpp b/platform/platformdrmgeneric.cpp
index 1aa8160..e2aa578 100644
--- a/platform/platformdrmgeneric.cpp
+++ b/platform/platformdrmgeneric.cpp
@@ -146,7 +146,7 @@ int DrmGenericImporter::ImportBuffer(buffer_handle_t handle, hwc_drm_bo_t *bo) {
   }
 
   for (int i = 1; i < HWC_DRM_BO_MAX_PLANES; i++) {
-    int fd = bo->prime_fds[i];
+    uint32_t fd = bo->prime_fds[i];
     if (fd != 0) {
       if (fd != bo->prime_fds[0]) {
         ALOGE("Multiplanar FBs are not supported by this version of composer");
