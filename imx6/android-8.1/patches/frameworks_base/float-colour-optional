Avoid fatal error "F OpenGLRenderer: Device claims wide gamut support, cannot find matching config"

From: Martin Fuzzey <martin.fuzzey@flowbird.group>
Date: 2018-07-25 18:52:53 +0200 

Do not exit with a fatal error if the EGL_EXT_pixel_format_float extenstion is supported
but no corresponding EGL config exists.

Just behave as if the extension did not exist in that case.

The prevents "F OpenGLRenderer: Device claims wide gamut support, cannot find matching config, error = EGL_SUCCESS"

I think this is actually an Android bug.
Nothing in the spec says that if the extension is supported by an implementation it must also be supported
by at least one config.
---
 libs/hwui/renderthread/EglManager.cpp |    5 +++--
 1 file changed, 3 insertions(+), 2 deletions(-)

diff --git a/libs/hwui/renderthread/EglManager.cpp b/libs/hwui/renderthread/EglManager.cpp
index 16d7736..38df0ae 100644
--- a/libs/hwui/renderthread/EglManager.cpp
+++ b/libs/hwui/renderthread/EglManager.cpp
@@ -223,7 +223,7 @@ void EglManager::loadConfigs() {
         numConfigs = 1;
         if (!eglChooseConfig(mEglDisplay, attribs16F, &mEglConfigWideGamut, numConfigs, &numConfigs)
                 || numConfigs != 1) {
-            LOG_ALWAYS_FATAL(
+            ALOGW(
                     "Device claims wide gamut support, cannot find matching config, error = %s",
                     eglErrorString());
         }
@@ -256,7 +256,8 @@ EGLSurface EglManager::createSurface(EGLNativeWindowType window, bool wideColorG
     initialize();
 
     wideColorGamut = wideColorGamut && EglExtensions.glColorSpace && EglExtensions.scRGB
-            && EglExtensions.pixelFormatFloat && EglExtensions.noConfigContext;
+            && EglExtensions.pixelFormatFloat && EglExtensions.noConfigContext
+            && (mEglConfigWideGamut != nullptr);
 
     // The color space we want to use depends on whether linear blending is turned
     // on and whether the app has requested wide color gamut rendering. When wide
